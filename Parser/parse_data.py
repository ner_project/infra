import time

def get_characters(url):
    # Get the list of characters by chapter
    browser = webdriver.Chrome()
    browser.get(url)
    character_dict = dict()
    elem = browser.find_element(By.CLASS_NAME, "mw-parser-output")
    #Locate character by chapter
    tables = elem.find_elements(By.TAG_NAME, 'table')
    for i, chapter in enumerate(tables):
        list_of_characters = []
        characters = chapter.find_elements(By.TAG_NAME,'a')
        for character in characters:
            if not character.get_attribute('title'):
                continue
            list_of_characters.append({'title': character.get_attribute('title'), 'url': character.get_attribute('href')})
        character_dict['chapter_' + str(i + 1)] = list_of_characters
  # Enrich characters with additional information
    for chapter in character_dict:
        for index, character in enumerate(character_dict[chapter]):
      # Rate limit sleep
            time.sleep(1)
      # Get the character page with selenium
            browser.get(character['url'])
            # Enrich aliases
            try:
                alias_div = browser.find_element(By.XPATH, "//div[@data-source = 'alias']")
                aliases = alias_div.find_elements(By.TAG_NAME, 'li')
                result = []
                for a in aliases:
          # Ignore under the cloak-guise and the name he told
                    if "disguise" in a.text or "the name he told" in a.text:
                        continue
                    alias = a.text.split('[')[0].split('(')[0].strip()
                    result.append(alias)
                character_dict[chapter][index]['aliases'] = result
            except:
                print('fail')
      # Enrich loyalties
            try:
                loyalty_div = browser.find_element(By.XPATH,"//div[@data-source = 'loyalty']")
                loyalties = loyalty_div.find_elements(By.TAG_NAME,'li')
                result = []
                for l in loyalties:
                    loyalty = l.text.split('[')[0].split('(')[0].strip()
                    result.append(loyalty)
                character_dict[chapter][index]['loyalty'] = result
            except:
                print('fail')
      # Enrich family relationships
            try:
                family_div = browser.find_element(By.XPATH,"//div[@data-source = 'family']")
                relationships = family_div.find_elements(By.TAG_NAME,'li')
                result = []
                for r in relationships:
                    rel = r.text.split('[')[0].split('(')[0].strip()
                    rel_type = r.text.split('(')[-1].split(')')[0].split('[')[0]
                    result.append({'person':rel, 'type': rel_type})
                character_dict[chapter][index]['family'] = result
            except:
                print('fail')
      # Enrich blood
            character_dict[chapter][index]['blood'] = enrich_single_item('blood')
            # Enrich nationality
            character_dict[chapter][index]['nationality'] = enrich_single_item('nationality')
            # Enrich species
            character_dict[chapter][index]['species'] = enrich_single_item('species')
            # Enrich house
            character_dict[chapter][index]['house'] = enrich_single_item('house')
            # Enrich gender
            character_dict[chapter][index]['gender'] = enrich_single_item('gender')
    return character_dict