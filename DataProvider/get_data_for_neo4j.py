from collections import Counter

def get_character_dict(chapter):
    super_list = list()
    dicts = [character_dict['chapter_' + str(i)] for i in range(1,chapter + 1)]
    for d in dicts:
        for item in d:
            super_list.append(item)
    return super_list


def coref_resolution(text):
    """Function that executes coreference resolution on a given text"""
    doc = nlp(text)
    # fetches tokens with whitespaces from spacy document
    tok_list = list(token.text_with_ws for token in doc)
    for cluster in doc._.coref_clusters:
        # get tokens from representative cluster name
        cluster_main_words = set(cluster.main.text.split(' '))
        for coref in cluster:
            if coref != cluster.main:  # if coreference element is not the representative element of that cluster
                if coref.text != cluster.main.text and bool(set(coref.text.split(' ')).intersection(cluster_main_words)) == False:
                    # if coreference element text and representative element text are not equal and none of the coreference element words are in representative element. This was done to handle nested coreference scenarios
                    tok_list[coref.start] = cluster.main.text + \
                        doc[coref.end-1].whitespace_
                    for i in range(coref.start+1, coref.end):
                        tok_list[i] = ""

    return "".join(tok_list)


def get_matcher_patterns(character):
    matcher_pattern = []
    stop_words = ['of', 'the', 'at', 'family', 'keeper', 'wizard', 'fat', 'de', 'hogwarts', 'hotel', 'owner', 'express']
    parts_of_name = [el for el in character['title'].split(' ') if len(el) > 2]
    # Append the whole pattern
    matcher_pattern.append([{"LOWER": n.lower(), "IS_TITLE": True} for n in parts_of_name])

    # Append parts of names
    if not "'" in character['title']:  # Skip names like Vernon Dursley's secretary
        for n in parts_of_name:
            if n.lower() in stop_words:  # Skip appending stop words
                continue
            matcher_pattern.append([{"LOWER": n.lower(), "IS_TITLE": True}])
            # Special case for Ronald Weasley -> Also add Ron
            if n == "Ronald":
                matcher_pattern.append([{"LOWER": "ron", "IS_TITLE": True}])
    return matcher_pattern


def get_distances(result, distance_threshold):
    #sort by start character
    result = sorted(result, key=lambda k: k['start'])
    compact_entities = []
    # Merge entities
    for entity in result:
        # If the same entity occurs, prolong the end
        if (len(compact_entities) > 0) and (compact_entities[-1]['string_id'] == entity['string_id']):
            compact_entities[-1]['end'] = entity['end']
        else:
            compact_entities.append(entity)
    distances = list()
    # Iterate over all entities
    for index, source in enumerate(compact_entities[:-1]):
        # Compare with entities that come after the given one
        for target in compact_entities[index + 1:]:
            if (source['string_id'] != target['string_id']) and (abs(source['end'] - target['start']) < distance_threshold):
                link = sorted([source['string_id'][0], target['string_id'][0]])
                distances.append(link)
            else:
                break
    # Count the number of interactions
    return Counter(map(tuple, distances))

def store_to_neo4j(distances):
    data = [{'source': el[0], 'target': el[1], 'weight': distances[el]} for el in distances]
    with driver.session() as session:
        session.run("""
        UNWIND $data as row
        MERGE (c:Character{name:row.source})
        MERGE (t:Character{name:row.target})
        MERGE (c)-[i:INTERACTS]-(t)
        SET i.weight = coalesce(i.weight,0) + row.weight
        """, {'data': data})def store_to_neo4j(distances):
    data = [{'source': el[0], 'target': el[1], 'weight': distances[el]} for el in distances]
    with driver.session() as session:
        session.run("""
        UNWIND $data as row
        MERGE (c:Character{name:row.source})
        MERGE (t:Character{name:row.target})
        MERGE (c)-[i:INTERACTS]-(t)
        SET i.weight = coalesce(i.weight,0) + row.weight
        """, {'data': data})
