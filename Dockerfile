FROM python:3-slim

ENV TZ=UTC

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

CMD mlflow server \
  --backend-store-uri postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@db:5432/${POSTGRES_DB}\
  --default-artifact-root ${ARTIFACT_ROOT} \
  --host 0.0.0.0